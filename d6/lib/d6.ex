defmodule D6 do

  def start(_, _) do
    Helper.run(&pt1/1, &pt2/1)
  end

  def pt1 input do
    input 
    |> IO.inspect
    |> parse_input
    |> IO.inspect(label: "parsed form:")
  end
  
  def pt2 input do
    input 
    |> IO.inspect
    #|> parse_input_pt2
    #|> IO.inspect(label: "parsed form:")
  end

  def parse_input(input) do
    [times, distances] = input 
                         |> String.split("\n", trim: true)
    t = times
        |> String.split(" ", trim: true)
        |> Helper.tail
        |> Helper.list_iparse

    d = distances
        |> String.split(" ", trim: true)
        |> Helper.tail
        |> Helper.list_iparse

    [times: t, distances: d]
  end

  # distance traveled = speed * time_r
  # speed = time_h
  # time_r = time - time_h
  # => dist = time_h * (time - time_h)
  # => dist = time*time_h - time_h^2         (( i.e. A*x - Bx^2  ))
  # max at derivative=0   =>    0 = time - 2*time_h
  # => 2*time_h = time => time_h = time/2
end  
