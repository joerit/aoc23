defmodule D5 do

  def start(_, _) do
    Helper.run(&pt1/1, &pt2/1)
  end

  def pt1 input do
    input 
    |> IO.inspect
    |> parse_input
    |> IO.inspect(label: "parsed form:")
    |> get_lowest_location_from_seed
  end
  
  def pt2 input do
    input 
    |> IO.inspect
    |> parse_input_pt2
    |> IO.inspect(label: "parsed form:")
    |> get_lowest_location_from_seed
  end
  
  ######################### PARSING
  def parse_input( input ) do
    [seeds | tail] = String.split(input, "\n\n", parts: 2, trim: true)
    [seeds: parse_seeds(seeds), maps: parse_maps(tail)]
  end
  
  def iparse(string) do
    case Integer.parse(string) do
      :error -> raise "tried to parse '#{string}'"
      {int, _} -> int
    end
  end
  def list_iparse(list) do
    list |> Enum.map(fn str -> iparse(str) end)
  end

  def parse_seeds("seeds: "<>line) do
    String.split(line, " ")
    |> _parse_seed
  end
  def _parse_seed([seed | tail]) do
    [iparse(seed) | _parse_seed(tail)]
  end
  def _parse_seed([seed]) do
    [iparse(seed)]
  end
  def _parse_seed([]) do
    []
  end
  
  def parse_maps ([maps]) do
    String.split(maps, "\n\n")
    |> Enum.map(&_parse_map/1)
  end
  def _parse_map (map) do
    [title | inputs] = String.split(map, "\n", trim: true)
    [from: from, to: to] = parse_title(title)
    maps = inputs
           |> Enum.map(fn line ->
             [dest, src, len] = String.split(line, " ") 
                                |> list_iparse
             [src: src, dest: dest, len: len]
           end)
           |> Enum.sort

    [from: from, to: to, maps: maps] 
  end

  def parse_title(title) do
    # seed-to-soil map:
    [from, _, to] = title
                    |> String.split(" ", parts: 2)
                    |> List.first
                    |> String.split("-")
    [from: from, to: to]
  end

  def parse_input_pt2( input ) do
    [seeds | tail] = String.split(input, "\n\n", parts: 2, trim: true)
    [seeds: parse_seeds_pt2(seeds), maps: parse_maps(tail)]
  end
  def parse_seeds_pt2("seeds: "<>line) do
    String.split(line, " ")
    |> Enum.chunk_every(2)
    |> IO.inspect(label: "parse_seeds_pt2")
    |> _parse_seed_pt2
  end
  def _parse_seed_pt2([[seed_start, seed_length] | tail]) do
    IO.inspect("")
    start = iparse(seed_start)
    length = iparse(seed_length)
    start..(start+length) ++ _parse_seed_pt2(tail)
  end
  def _parse_seed_pt2([[seed_start, seed_length]]) do
    start = iparse(seed_start)
    length = iparse(seed_length)
    start..(start+length)
  end
  def _parse_seed_pt2([]) do
    []
  end
  ########################## PARSING END

  def get_lowest_location_from_seed(input) do
    Enum.map(input[:seeds], fn seed -> get_location(seed, input[:maps]) end)
    |> Enum.sort()
    |> List.first
  end

  def get_location(seed, [map | map_tail]) do
    translate(seed, map)
    |> get_location(map_tail)
  end
  def get_location(seed, [map]) do
    translate(seed, map)
  end
  def get_location(seed, []) do
    seed
  end
  
  def translate(from, [from: _, to: _, maps: maps]) do
    # is our src in the mapping?
    case Enum.filter(maps, 
      fn [src: src, dest: _, len: len] -> (from > src) and (from < src+len) end) 
      do
      [[src: src, dest: dest, len: len]] -> from - src + dest
      [] -> from
      _ -> raise "panick in translate on '#{from}' -> '#{maps}'"
    end
  end
end

