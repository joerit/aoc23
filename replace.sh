U1=$(echo $1 | tr '[:lower:]' '[:upper:]')
U2=$(echo $2 | tr '[:lower:]' '[:upper:]')
L1=$(echo $1 | tr '[:upper:]' '[:lower:]')
L2=$(echo $2 | tr '[:upper:]' '[:lower:]')
files=$(rg -il $1)
echo "replacing $1 with $2 in $files"

parallel sed -i '' "s/$U1/$U2/g" ::: $files
parallel sed -i '' "s/$L1/$L2/g" ::: $files

mv ./lib/${L1}.ex ./lib/${L2}.ex
mv ./test/${L1}_test.exs ./test/${L2}_test.exs
