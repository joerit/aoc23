defmodule D1 do
  @moduledoc """
  Documentation for `D1`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> D1.hello()
      :world

  """
  def parse line do
    parse_left( pad(line) ) * 10 + parse_right( pad(String.reverse(line)) )
    |> IO.inspect
  end
  
  def parse_left "...." do 
    0
  end
  def parse_left <<h1, h2, h3, h4, h5, rest::binary>> do
    IO.inspect([h1, h2, h3, h4, h5, rest], label: "PL")
    case [h1, h2, h3, h4, h5] do
      [?o, ?n, ?e, _, _] -> IO.inspect(1)
      [?t, ?w, ?o, _, _] -> IO.inspect(2)
      [?t, ?h, ?r, ?e, ?e] -> IO.inspect(3)
      [?f, ?o, ?u, ?r, _] -> IO.inspect(4)
      [?f, ?i, ?v, ?e, _] -> IO.inspect(5)
      [?s, ?i, ?x, _, _] -> IO.inspect(6)
      [?s, ?e, ?v, ?e, ?n] -> IO.inspect(7)
      [?e, ?i, ?g, ?h, ?t] -> IO.inspect(8)
      [?n, ?i, ?n, ?e, _] -> IO.inspect(9)
      [dig, _, _, _, _] when dig in ?0..?9 -> IO.inspect(dig - ?0)
      _ -> parse_left(<<h2, h3, h4, h5, rest::binary>>)
    end
  end
  def parse_right "...." do 
    0
  end
  # second funct because i can't be arsed to reverse this sensibly
  def parse_right <<h1, h2, h3, h4, h5, rest::binary>> do
    IO.inspect([h1, h2, h3, h4, h5, rest], label: "PR")
    case [h1, h2, h3, h4, h5] do
      [?e, ?n, ?o, _, _] -> 1
      [?o, ?w, ?t, _, _] -> 2
      [?e, ?e, ?r, ?h, ?t] -> 3
      [?r, ?u, ?o, ?f, _] -> 4
      [?e, ?v, ?i, ?f, _] -> 5
      [?x, ?i, ?s, _, _] -> 6
      [?n, ?e, ?v, ?e, ?s] -> 7
      [?t, ?h, ?g, ?i, ?e] -> 8
      [?e, ?n, ?i, ?n, _] -> 9
      [dig, _, _, _, _] when dig in ?0..?9 -> dig - ?0 
      _ -> parse_right(<<h2, h3, h4, h5, rest::binary>>)
    end
  end



  def parse_lines [head] do
    [parse(head)]
  end
  def parse_lines [head | tail] do
    [parse(head) | parse_lines(tail) ]
  end

  def sum [one] do
    one
  end
  def sum [head | tail] do
    head + sum(tail)
  end
  def pad(str) do
    str<>"...."
  end

  def main(_) do
    File.stream!("input")
    |> Enum.to_list
    |> parse_lines
    |> sum
    |> IO.inspect
  end
end
